import { GITLAB } from "../mutation-types";
import Gitlab from "../../helpers/gitlab";

export default {
  state: {
    projects: []
  },
  mutations: {
    [GITLAB.PROJECT.SET_PROJECTS] (state, params) {
      state.projects = params
    },
  },
  actions: {
    async loadProjects ({ commit }) {
      let gitlab = new Gitlab()
      await gitlab.reloadAuth()
      const projects = await gitlab.project().getProjects({
        membership: true,
        per_page: 100
      })
      commit(GITLAB.PROJECT.SET_PROJECTS, projects)
    },
  },
  getters: {
    projects: state => state.projects
  }
}
