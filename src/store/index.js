require('es6-promise/auto');

import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

import gui from './gui'
import gitlab from './gitlab'

export default new Vuex.Store({
  modules: {
    gui,
    gitlab,
  }
})


