import Api from "./api";

export default class Project extends Api {
  async getProjects (options = { membership: true }) {
    return await this.get('/projects', options)
  }
}
