import { GUI } from "../mutation-types";

export default {
  state: {
    visibility: false
  },
  mutations: {
    [GUI.LOADING.SET_VISIBILITY] (state, visibility) {
      state.visibility = visibility
    },
  },
  actions: {
    show ({ commit }) {
      commit(GUI.LOADING.SET_VISIBILITY, true);
    },
    hide ({ commit }) {
      commit(GUI.LOADING.SET_VISIBILITY, false);
    },
    toggle ({ commit, state }) {
      commit(GUI.LOADING.SET_VISIBILITY, !state.visibility)
    }
  },
  getters: {
    loadingVisible: state => state.visibility
  }
}
