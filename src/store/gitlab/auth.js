import { GITLAB } from "../mutation-types";
import Gitlab from "../../helpers/gitlab";

export default {
  state: {
    domain: null,
    token: null,
  },
  mutations: {
    [GITLAB.AUTH.SET_AUTH] (state, params) {
      if (params.domain) {
        state.domain = params.domain
      }
      if (params.token) {
        state.token = params.token
      }
    },
    [GITLAB.AUTH.RESET_AUTH] (state) {
      state.domain = null
      state.token = null
    },
  },
  actions: {
    async loadAuth ({ commit }) {
      let gitlab = new Gitlab()
      let credential = await gitlab.reloadAuth()
      await commit(GITLAB.AUTH.SET_AUTH, {
        domain: credential.domain,
        token: credential.token
      });
    },
    async setAuth ({ commit }, params) {
      let gitlab = new Gitlab()
      await gitlab.setAuth({
        domain: params.domain,
        token: params.token
      })
      await commit(GITLAB.AUTH.SET_AUTH, params);
    },
    async resetAuth ({ commit }) {
      let gitlab = new Gitlab()
      await gitlab.removeAuth()
      await commit(GITLAB.AUTH.RESET_AUTH);
    },
    validateAuth ({commit}, params) {
      let gitlab = new Gitlab()
      return gitlab.validateAuth(params.domain, params.token);
    }
  },
  getters: {
    domain: state => state.domain,
    token: state => state.domain
  }
}
