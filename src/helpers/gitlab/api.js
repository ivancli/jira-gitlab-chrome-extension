const ACCESS_TOKEN_PARAMETER_NAME = 'access_token'

export default class Api {
  constructor ({ domain, token }) {
    this.version = 'v4'
    this.domain = domain
    this.token = token
  }

  /**
   * @param path
   * @param params
   * @param isJson
   * @returns {Promise<any>}
   */
  async get (path, params = {}, isJson = true) {
    let url = this._assembleUrlWithAccessToken(path, params)
    let response = await fetch(url)

    if (response.status >= 400) {
      throw 'Unable to fetch remote content'
    }

    return true === isJson ? response.json() : response
  }

  /**
   * @param path
   * @param params
   * @returns {string}
   * @private
   */
  _assembleUrlWithAccessToken (path, params = {}) {
    let urlString = this.domain + '/api/' + this.version + path;
    let url = new URL(urlString)

    for (const key in params) {
      url.searchParams.set(key, params[key])
    }

    url.searchParams.set(ACCESS_TOKEN_PARAMETER_NAME, this.token)

    return url.toString()
  }

}
