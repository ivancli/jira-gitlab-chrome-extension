import { GITLAB } from "../mutation-types";
import Gitlab from "../../helpers/gitlab";

export default {
  state: {
    branches: []
  },
  mutations: {
    [GITLAB.BRANCH.SET_BRANCHES] (state, params) {
      state.branches = params
    },
  },
  actions: {
    async loadBranches ({ commit }, params) {
      let gitlab = new Gitlab()
      await gitlab.reloadAuth()
      const branches = await gitlab.branch().getBranches(params.project_id)
      commit(GITLAB.BRANCH.SET_BRANCHES, branches)
    },
  },
  getters: {
    branches: state => state.branches
  }
}
