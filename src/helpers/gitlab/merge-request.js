import Api from "./api";

export default class MergeRequest extends Api {
  async getMergeRequests (options = { state: 'opened' }) {
    return await this.get('/merge_requests', options)
  }
}
