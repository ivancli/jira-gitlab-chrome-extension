export const GUI = {
  LOADING: {
    SHOW: 'gui/show',
    HIDE: 'gui/hide',
    TOGGLE: 'gui/toggle',
  }
}

export const GITLAB = {
  AUTH: {
    LOAD_AUTH: 'gitlab/loadAuth',
    SET_AUTH: 'gitlab/setAuth',
    RESET_AUTH: 'gitlab/resetAuth',
    VALIDATE_AUTH: 'gitlab/validateAuth',
  },
  PROJECT: {
    LOAD_PROJECTS: 'gitlab/loadProjects',
  },
  MERGE_REQUEST: {
    LOAD_MERGE_REQUESTS: 'gitlab/loadMergeRequests',
  },
  BRANCH: {
    LOAD_BRANCHES: 'gitlab/loadBranches',
  },
  COMMIT: {
    LOAD_COMMITS: 'gitlab/loadCommits',
  },
  JIRA_STAT: {
    INIT: 'gitlab/jiraStat/init',
    SET_JIRA_ISSUE_NUMBER: 'gitlab/jiraStat/setJiraIssueNumber',
    SET_PROJECT: 'gitlab/jiraStat/setProject',
    LOAD_BRANCHES: 'gitlab/jiraStat/loadBranches',
    LOAD_COMMITS: 'gitlab/jiraStat/loadCommits',
    LOAD_MERGE_REQUESTS: 'gitlab/jiraStat/loadMergeRequests',
  }
}
