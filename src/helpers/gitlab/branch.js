import Api from "./api";

export default class Branch extends Api {
  async getBranches (projectId, options = {}) {
    return await this.get('/projects/' + projectId + '/repository/branches', options)
  }
}
