import { GITLAB } from "../mutation-types";
import Gitlab from "../../helpers/gitlab";

export default {
  state: {
    mergeRequests: []
  },
  mutations: {
    [GITLAB.MERGE_REQUEST.SET_MERGE_REQUESTS] (state, params) {
      state.mergeRequests = params
    },
  },
  actions: {
    async loadMergeRequests ({ commit }, params) {
      let gitlab = new Gitlab()
      await gitlab.reloadAuth()
      const mergeRequests = await gitlab.mergeRequest().getMergeRequests(params)
      commit(GITLAB.MERGE_REQUEST.SET_MERGE_REQUESTS, mergeRequests)
    },
  },
  getters: {
    mergeRequests: state => state.mergeRequests
  }
}
