import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

import App from './App'
import '../styles/app.scss'

import store from '../store'

new Vue({
  el: '#app',
  render: h => h(App),
  store
})
