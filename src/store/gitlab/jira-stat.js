import { GITLAB as MUTATION_GITLAB } from "../mutation-types";
import { GITLAB as ACTION_GITLAB } from '../action-types'
import Gitlab from "../../helpers/gitlab";

import Vue from 'vue'

export default {
  namespaced: true,
  state: {
    jiraIssueNumber: null,
    project: null,
    branches: [],
    commits: [],
    mergeRequests: [],
    loading: {
      branches: false,
      commits: false,
      mergeRequests: false,
    }
  },
  mutations: {
    [MUTATION_GITLAB.JIRA_STAT.SET_JIRA_ISSUE_NUMBER] (state, params) {
      state.jiraIssueNumber = params
    },
    [MUTATION_GITLAB.JIRA_STAT.SET_PROJECT] (state, params) {
      state.project = params
    },
    [MUTATION_GITLAB.JIRA_STAT.SET_BRANCHES] (state, params) {
      state.branches = params
    },
    [MUTATION_GITLAB.JIRA_STAT.SET_COMMITS] (state, params) {
      state.commits = params
    },
    [MUTATION_GITLAB.JIRA_STAT.SET_MERGE_REQUESTS] (state, params) {
      state.mergeRequests = params
    },
    [MUTATION_GITLAB.JIRA_STAT.SET_LOADING.BRANCHES] (state, params) {
      Vue.set(state.loading, 'branches', params)
    },
    [MUTATION_GITLAB.JIRA_STAT.SET_LOADING.COMMITS] (state, params) {
      Vue.set(state.loading, 'commits', params)
    },
    [MUTATION_GITLAB.JIRA_STAT.SET_LOADING.MERGE_REQUESTS] (state, params) {
      Vue.set(state.loading, 'mergeRequests', params)
    },
  },
  actions: {
    async init ({ dispatch, commit, state }, params) {
      await commit(MUTATION_GITLAB.JIRA_STAT.SET_JIRA_ISSUE_NUMBER, params.jiraIssueNumber)
      await commit(MUTATION_GITLAB.JIRA_STAT.SET_PROJECT, params.project)
      if (null !== state.project && null !== state.jiraIssueNumber) {
        await dispatch('loadBranches')
      }
    },
    async setJiraIssueNumber ({ dispatch, commit, state }, params) {
      if (state.jiraIssueNumber !== params.jiraIssueNumber) {
        await commit(MUTATION_GITLAB.JIRA_STAT.SET_JIRA_ISSUE_NUMBER, params.jiraIssueNumber)
        if (null !== state.project && null !== state.jiraIssueNumber) {
          await dispatch('loadBranches')
        }
      }
    },
    async setProject ({ dispatch, commit, state }, params) {
      if (state.project !== params.project) {
        await commit(MUTATION_GITLAB.JIRA_STAT.SET_PROJECT, params.project)
        if (null !== state.project && null !== state.jiraIssueNumber) {
          await dispatch('loadBranches')
        }
      }
    },
    async loadBranches ({ dispatch, commit, state }) {
      commit(MUTATION_GITLAB.JIRA_STAT.SET_LOADING.BRANCHES, true)
      let gitlab = new Gitlab()
      await gitlab.reloadAuth()
      const branches = await gitlab.branch().getBranches(state.project.id, {
        search: state.jiraIssueNumber
      })
      if (state.branches !== branches) {
        commit(MUTATION_GITLAB.JIRA_STAT.SET_BRANCHES, branches)
        await dispatch('loadCommits')
        await dispatch('loadMergeRequests')
      }
      commit(MUTATION_GITLAB.JIRA_STAT.SET_LOADING.BRANCHES, false)
    },
    async loadCommits ({ commit, state }, params) {
      commit(MUTATION_GITLAB.JIRA_STAT.SET_LOADING.COMMITS, true)
      let gitlab = new Gitlab()
      await gitlab.reloadAuth()
      let commits = []
      for (let key in state.branches) {
        if (state.branches.hasOwnProperty(key)) {
          commits = commits.concat(await gitlab.commit().getCommits(state.project.id, state.branches[key].name, {
            since: state.project.created_at // todo need to fix this, now it's loading all commits of a branch rather than the relevant ones
          }))
        }
      }
      await commit(MUTATION_GITLAB.JIRA_STAT.SET_COMMITS, commits)
      commit(MUTATION_GITLAB.JIRA_STAT.SET_LOADING.COMMITS, false)
    },
    async loadMergeRequests ({ commit, state }, params) {
      commit(MUTATION_GITLAB.JIRA_STAT.SET_LOADING.MERGE_REQUESTS, true)
      let gitlab = new Gitlab()
      await gitlab.reloadAuth()
      let mergeRequests = []
      for (let key in state.branches) {
        if (state.branches.hasOwnProperty(key)) {
          mergeRequests = mergeRequests.concat(await gitlab.mergeRequest().getMergeRequests({
            source_branch: state.branches[key].name
          }))
        }
      }
      commit(MUTATION_GITLAB.JIRA_STAT.SET_MERGE_REQUESTS, mergeRequests)
      commit(MUTATION_GITLAB.JIRA_STAT.SET_LOADING.MERGE_REQUESTS, false)
    }
  },
  getters: {
    project: state => state.project,
    branches: state => state.branches,
    commits: state => state.commits,
    mergeRequests: state => state.mergeRequests,
    loading: state => state.loading,
  }
}
