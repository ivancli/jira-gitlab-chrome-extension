import Api from "./api";

export default class Commit extends Api {
  async getCommits (projectId, branchName, options = {}) {
    options.ref_name = branchName
    return await this.get('/projects/' + projectId + '/repository/commits', options)
  }
}
