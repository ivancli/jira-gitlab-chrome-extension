import { STORAGE_KEY } from "../constants";
import Project from "./project";
import MergeRequest from "./merge-request";
import Branch from "./branch";
import Commit from "./commit";

export default class Index {
  constructor () {
    this._domain = null
    this._token = null
  }

  /**
   * @param domain
   * @param token
   * @returns {boolean}
   */
  async validateAuth (domain, token) {
    this._domain = domain
    this._token = token

    try {
      await this.project().getProjects()
    } catch (e) {
      return false;
    }

    return true;
  }

  /**
   * @returns {Promise<unknown>}
   */
  reloadAuth () {
    return new Promise((resolve, reject) => {
      chrome.storage.sync.get([STORAGE_KEY.PERSONAL_ACCESS_TOKEN, STORAGE_KEY.GITLAB_DOMAIN], data => {
        this._domain = data[STORAGE_KEY.GITLAB_DOMAIN]
        this._token = data[STORAGE_KEY.PERSONAL_ACCESS_TOKEN]
        resolve({
          domain: this._domain,
          token: this._token
        })
      });
    })
  }

  /**
   * @param domain
   * @param token
   * @returns {Promise<unknown>}
   */
  setAuth ({ domain, token }) {
    return new Promise((resolve, reject) => {
      this._domain = domain
      this._token = token
      chrome.storage.sync.set({
        [STORAGE_KEY.GITLAB_DOMAIN]: this._domain,
        [STORAGE_KEY.PERSONAL_ACCESS_TOKEN]: this._token,
      }, () => {
        resolve()
      });
    })
  }

  /**
   * @returns {Promise<unknown>}
   */
  removeAuth () {
    return new Promise((resolve, reject) => {
      chrome.storage.sync.remove([STORAGE_KEY.GITLAB_DOMAIN, STORAGE_KEY.PERSONAL_ACCESS_TOKEN], () => {
        resolve()
      });
    })
  }

  isAuthUnset () {
    return this._domain === null || this._token === null;
  }

  handleUnauth () {
    if (this.isAuthUnset()) {
      throw 'Credential is not set.'
    }
  }

  /**
   * @returns {Project}
   */
  project () {
    this.handleUnauth()

    return new Project({
      domain: this._domain,
      token: this._token,
    })
  }

  /**
   * @returns {MergeRequest}
   */
  mergeRequest () {
    this.handleUnauth()

    return new MergeRequest({
      domain: this._domain,
      token: this._token,
    })
  }

  /**
   * @returns {Branch}
   */
  branch () {
    this.handleUnauth()

    return new Branch({
      domain: this._domain,
      token: this._token,
    })
  }

  /**
   * @returns {Commit}
   */
  commit () {
    this.handleUnauth()

    return new Commit({
      domain: this._domain,
      token: this._token
    })
  }
}
