import auth from './auth';
import project from './project';
import mergeRequest from './merge-request'
import branch from "./branch";
import commit from "./commit";
import jiraStat from './jira-stat'

export default {
  namespaced: true,
  modules: {
    auth,
    project,
    mergeRequest,
    branch,
    commit,
    jiraStat
  }
}
