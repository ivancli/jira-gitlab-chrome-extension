import loading from "./loading";

export default {
  namespaced: true,
  modules: {
    loading,
  }
}
