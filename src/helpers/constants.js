export const STORAGE_KEY = {
  GITLAB_DOMAIN: 'gitlabDomain',
  PERSONAL_ACCESS_TOKEN: 'personalAccessToken',
  SELECTED_PROJECT: 'selectedProject',
}
