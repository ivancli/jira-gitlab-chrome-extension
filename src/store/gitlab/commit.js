import { GITLAB } from "../mutation-types";
import Gitlab from "../../helpers/gitlab";

export default {
  state: {
    commits: []
  },
  mutations: {
    [GITLAB.COMMIT.SET_COMMITS] (state, params) {
      state.commits = params
    },
  },
  actions: {
    async loadCommits ({ commit }, params) {
      let gitlab = new Gitlab()
      await gitlab.reloadAuth()
      let commits = []
      if (typeof params.branch_name === 'object') {
        for (let key in params.branch_name) {
          let result = await gitlab.commit().getCommits(params.project_id, params.branch_name[key], {
            first_parent: true
          })
          commits = commits.concat(result)
        }
        commits = commits.filter((value, index, self) => self.indexOf(value) === index)
      } else {
        commits = commits.concat(await gitlab.commit().getCommits(params.project_id, params.branch_name, {
          first_parent: true
        }))
      }
      commit(GITLAB.COMMIT.SET_COMMITS, commits)
    },
  },
  getters: {
    commits: state => state.commits
  }
}
